﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountMeIn.Models.TeamViewModels
{
    public class CreateTeamViewModel
    {
        public string Name { get; set; }
    }
}
