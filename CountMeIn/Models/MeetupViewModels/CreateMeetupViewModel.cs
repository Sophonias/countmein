﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountMeIn.Models.MeetupViewModels
{
    public class CreateMeetupViewModel
    {
        public DateTime Time { get; set; }
        public string Location { get; set; }
        public string Description { get; set; }
        public Guid LeagueId { get; set; }
    }
}
