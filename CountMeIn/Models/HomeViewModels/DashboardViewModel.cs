﻿using CountMeIn.AppModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountMeIn.Models.HomeViewModels
{
    public class DashboardViewModel
    {
        public string UserId { get; set; }
        public List<League> LeagueList { get; set; }        
    }
}
