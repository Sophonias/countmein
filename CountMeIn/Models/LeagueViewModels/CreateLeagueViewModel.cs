﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CountMeIn.Models.LeagueViewModels
{
    public class CreateLeagueViewModel
    {
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }
    }
}
