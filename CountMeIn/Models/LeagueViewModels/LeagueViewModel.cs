﻿using CountMeIn.AppModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountMeIn.Models.LeagueViewModels
{
    public class LeagueViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public List<CountMeInUser> Players { get; set; }
        public List<CountMeInUser> Organizers { get; set; }
        public List<Meetup> Meetups { get; set; }
    }
}
