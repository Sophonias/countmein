﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using CountMeIn.Models;
using CountMeIn.Data.DataModels;

namespace CountMeIn.Data
{
    public class CountMeInDbContext : IdentityDbContext<CountMeInUser>
    {
        public CountMeInDbContext(DbContextOptions<CountMeInDbContext> options)
            : base(options)
        {
        }

        public DbSet<LeagueModel> Leagues { get; set; }
        public DbSet<TeamModel> Teams { get; set; }
        public DbSet<MeetupModel> Meetups { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<LeagueModel>().HasKey(m => m.Id);
            builder.Entity<TeamModel>().HasKey(m => m.Id);
            builder.Entity<MeetupModel>().HasKey(m => m.Id);
            
            builder.Entity<LeagueModel>().Property(d => d.League).HasColumnType("json").HasColumnName("Data");
            builder.Entity<TeamModel>().Property(d => d.Team).HasColumnType("json").HasColumnName("Data");
            builder.Entity<MeetupModel>().Property(d => d.Meetup).HasColumnType("json").HasColumnName("Data");

            // shadow properties
            builder.Entity<LeagueModel>().Property<DateTime>("UpdatedTimestamp");
            builder.Entity<TeamModel>().Property<DateTime>("UpdatedTimestamp");
            builder.Entity<MeetupModel>().Property<DateTime>("UpdatedTimestamp");

            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }

        public override int SaveChanges()
        {
            ChangeTracker.DetectChanges();

            UpdateUpdatedProperty<LeagueModel>();
            UpdateUpdatedProperty<TeamModel>();
            UpdateUpdatedProperty<MeetupModel>();

            return base.SaveChanges();
        }

        private void UpdateUpdatedProperty<T>() where T : class
        {
            var modifiedSourceInfo =
                ChangeTracker.Entries<T>()
                    .Where(e => e.State == EntityState.Added || e.State == EntityState.Modified);

            foreach (var entry in modifiedSourceInfo)
            {
                entry.Property("UpdatedTimestamp").CurrentValue = DateTime.UtcNow;
            }
        }
    }
}