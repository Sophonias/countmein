﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CountMeIn.Data.Migrations
{
    public partial class Updatingdatabasemodels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Leagues_LeagueId",
                table: "AspNetUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Leagues_LeagueId1",
                table: "AspNetUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Teams_TeamId",
                table: "AspNetUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_Leagues_Teams_TeamId",
                table: "Leagues");

            migrationBuilder.DropForeignKey(
                name: "FK_Meetups_Leagues_LeagueId",
                table: "Meetups");

            migrationBuilder.DropForeignKey(
                name: "FK_Teams_Leagues_Id",
                table: "Teams");

            migrationBuilder.DropIndex(
                name: "IX_Meetups_LeagueId",
                table: "Meetups");

            migrationBuilder.DropIndex(
                name: "IX_Leagues_TeamId",
                table: "Leagues");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_LeagueId",
                table: "AspNetUsers");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_LeagueId1",
                table: "AspNetUsers");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_TeamId",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Teams");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "Meetups");

            migrationBuilder.DropColumn(
                name: "LeagueId",
                table: "Meetups");

            migrationBuilder.DropColumn(
                name: "Time",
                table: "Meetups");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Leagues");

            migrationBuilder.DropColumn(
                name: "TeamId",
                table: "Leagues");

            migrationBuilder.DropColumn(
                name: "LeagueId",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "LeagueId1",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "TeamId",
                table: "AspNetUsers");

            migrationBuilder.RenameColumn(
                name: "Location",
                table: "Meetups",
                newName: "Data");

            migrationBuilder.AddColumn<string>(
                name: "Data",
                table: "Teams",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Data",
                table: "Leagues",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Data",
                table: "Teams");

            migrationBuilder.DropColumn(
                name: "Data",
                table: "Leagues");

            migrationBuilder.RenameColumn(
                name: "Data",
                table: "Meetups",
                newName: "Location");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Teams",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Meetups",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "LeagueId",
                table: "Meetups",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Time",
                table: "Meetups",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Leagues",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<Guid>(
                name: "TeamId",
                table: "Leagues",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "LeagueId",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "LeagueId1",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "TeamId",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Meetups_LeagueId",
                table: "Meetups",
                column: "LeagueId");

            migrationBuilder.CreateIndex(
                name: "IX_Leagues_TeamId",
                table: "Leagues",
                column: "TeamId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_LeagueId",
                table: "AspNetUsers",
                column: "LeagueId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_LeagueId1",
                table: "AspNetUsers",
                column: "LeagueId1");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_TeamId",
                table: "AspNetUsers",
                column: "TeamId");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Leagues_LeagueId",
                table: "AspNetUsers",
                column: "LeagueId",
                principalTable: "Leagues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Leagues_LeagueId1",
                table: "AspNetUsers",
                column: "LeagueId1",
                principalTable: "Leagues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Teams_TeamId",
                table: "AspNetUsers",
                column: "TeamId",
                principalTable: "Teams",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Leagues_Teams_TeamId",
                table: "Leagues",
                column: "TeamId",
                principalTable: "Teams",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Meetups_Leagues_LeagueId",
                table: "Meetups",
                column: "LeagueId",
                principalTable: "Leagues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Teams_Leagues_Id",
                table: "Teams",
                column: "Id",
                principalTable: "Leagues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
