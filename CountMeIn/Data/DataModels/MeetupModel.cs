﻿using CountMeIn.AppModels;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CountMeIn.Data.DataModels
{
    public class MeetupModel
    {
        [Key]
        public Guid Id { get; set; }

        internal string Meetup { get; set; }

        [NotMapped]
        public Meetup Data
        {
            get { return this.Meetup == null ? null : JsonConvert.DeserializeObject<Meetup>(Meetup); }
            set { Meetup = JsonConvert.SerializeObject(value); }
        }
    }
}
