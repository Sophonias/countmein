﻿using CountMeIn.AppModels;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CountMeIn.Data.DataModels
{
    public class LeagueModel
    {
        [Key]
        public Guid Id { get; set; }

        internal string League { get; set; }

        [NotMapped]
        public League Data
        {
            get { return this.League == null ? null : JsonConvert.DeserializeObject<League>(League); }
            set { League = JsonConvert.SerializeObject(value);  }
        }
    }
}
