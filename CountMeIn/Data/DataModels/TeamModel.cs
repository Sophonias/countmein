﻿using CountMeIn.AppModels;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CountMeIn.Data.DataModels
{
    public class TeamModel
    {
        [Key]
        public Guid Id { get; set; }

        internal string Team { get; set; }

        [NotMapped]
        public Team Data
        {
            get { return this.Team == null ? null : JsonConvert.DeserializeObject<Team>(Team); }
            set { Team = JsonConvert.SerializeObject(value); }
        }
    }
}
