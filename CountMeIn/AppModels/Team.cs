﻿using CountMeIn.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CountMeIn.AppModels
{
    public class Team
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }

        [ForeignKey("Id")]
        public League CurrentLeague { get; set; }

        public List<CountMeInUser> Members { get; set; }
        public List<League> League { get; set; }
        

    }
}
