﻿using CountMeIn.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CountMeIn.AppModels
{
    public class League
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }

        public List<CountMeInUser> Players { get; set; }
        public List<CountMeInUser> Organizers { get; set; }
        public List<Meetup> Meetups { get; set; }
    }
}
