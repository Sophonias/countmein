﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CountMeIn.AppModels
{
    public class Meetup
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public DateTime Time { get; set; }
        public string Location { get; set; }
        public string Description { get; set; }
        public League League { get; set; }
        public List<Team> AvailableTeams { get; set; }

    }
}
