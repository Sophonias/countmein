﻿using System;
using System.Threading.Tasks;
using CountMeIn.AppModels;
using CountMeIn.Data.DataModels;
using CountMeIn.Models;
using CountMeIn.Models.LeagueViewModels;
using CountMeIn.Repositories;
using CountMeIn.Repositories.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CountMeIn.Controllers
{
    public class LeagueController : Controller
    {
        LeagueRepository repo;
        private readonly UserManager<CountMeInUser> _userManager;

        public LeagueController(
            ILeagueRepository leagueRepo,
            UserManager<CountMeInUser> userManager)
        {
            repo = (LeagueRepository)leagueRepo;
        }


        // GET: /<controller>/
        public IActionResult Index(Guid LeagueId)
        {            
            var league = (repo.GetById(LeagueId)).Result;
            var model = new LeagueViewModel()
            {
                Id = league.Id,
                Name = league.Data.Name,
                Meetups = league.Data?.Meetups,
                Organizers = league.Data?.Organizers,
                Players = league.Data?.Players
            };

            return View(model);
        }


        [HttpGet]
        public IActionResult Create()
        {
            var model = new CreateLeagueViewModel();

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreateLeagueViewModel model)
        {
            if (ModelState.IsValid)
            {
                var appModel = new League
                {
                    Name = model.Name,
                    Id = Guid.NewGuid()                    
                };

                var user = await _userManager.GetUserAsync(User);
                appModel.Organizers.Add(user);
                appModel.Players.Add(user);

                await repo.Add(new LeagueModel { Id = appModel.Id, Data = appModel});

                return RedirectToAction("Index", "League", appModel.Id);
            }
            return View(model);
        }
    }
}
