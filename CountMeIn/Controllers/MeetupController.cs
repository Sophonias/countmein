﻿using System;
using System.Threading.Tasks;
using CountMeIn.AppModels;
using CountMeIn.Data.DataModels;
using CountMeIn.Models.MeetupViewModels;
using CountMeIn.Repositories;
using CountMeIn.Repositories.Interfaces;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CountMeIn.Controllers
{
    public class MeetupController : Controller
    {
        MeetupRepository repo;
        LeagueRepository leagueRepo;

        public MeetupController(IMeetupRepository repo, ILeagueRepository leagueRepo)
        {
            this.repo = (MeetupRepository)repo;
            this.leagueRepo = (LeagueRepository)leagueRepo;
        }


        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Create(Guid leagueId)
        {
            var model = new CreateMeetupViewModel();
            model.LeagueId = leagueId;
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreateMeetupViewModel model)
        {
            if (ModelState.IsValid)
            {
                var appModel = new Meetup
                {
                    Id = Guid.NewGuid(),
                    Time = model.Time,
                    Location = model.Location,
                    Description = model.Description
                };
                var league = (leagueRepo.GetById(model.LeagueId)).Result;
                appModel.League = league.Data;

                await repo.Add(new MeetupModel { Id = appModel.Id, Data = appModel });
            }

            return View(model);
        }
    }
}
