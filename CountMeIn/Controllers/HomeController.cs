﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CountMeIn.Models;
using CountMeIn.Models.HomeViewModels;
using CountMeIn.Repositories;
using CountMeIn.Repositories.Interfaces;
using Microsoft.AspNetCore.Identity;

namespace CountMeIn.Controllers
{
    public class HomeController : Controller
    {
        LeagueRepository leagueRepo;
        private readonly UserManager<CountMeInUser> _userManager;

        public HomeController(
            ILeagueRepository leagueRepo,
            UserManager<CountMeInUser> userManager
            )
        {
            this.leagueRepo = (LeagueRepository)leagueRepo;
            _userManager = userManager;
        }

        [HttpGet]
        public async Task<IActionResult> Dashboard()
        {
            var user = await _userManager.GetUserAsync(User);
            var leagueModelList = leagueRepo.GetAll().ToList();
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var leagues = leagueModelList.Where(l => l.Data.Organizers.Any(d => d.Id == user.Id)).Select(l => l.Data);

            var model = new DashboardViewModel
            {
                UserId = user.Id,
                LeagueList = leagues?.ToList()
            };            
            

            return View(model);
        }


        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "This is a small application used to simplify scheduling our soccer games after work.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
