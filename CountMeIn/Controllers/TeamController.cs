﻿using System;
using System.Threading.Tasks;
using CountMeIn.AppModels;
using CountMeIn.Data.DataModels;
using CountMeIn.Models.TeamViewModels;
using CountMeIn.Repositories;
using CountMeIn.Repositories.Interfaces;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CountMeIn.Controllers
{
    public class TeamController : Controller
    {
        TeamRepository repo;

        public TeamController(ITeamRepository repo)
        {
            this.repo = (TeamRepository)repo;
        }
        
        
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new CreateTeamViewModel();

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreateTeamViewModel model)
        {
            if(ModelState.IsValid)
            {
                var appModel = new Team
                {
                    Name = model.Name,
                    Id = Guid.NewGuid()
                };

                await repo.Add(new TeamModel { Id = appModel.Id, Data = appModel });
            }

            return View(model);
        }
    }
}
