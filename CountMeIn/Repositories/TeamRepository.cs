﻿using CountMeIn.Data;
using CountMeIn.Data.DataModels;
using CountMeIn.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountMeIn.Repositories
{
    public class TeamRepository : BaseRepository<TeamModel>, ITeamRepository
    {
        CountMeInDbContext db;

        public TeamRepository(CountMeInDbContext context) : base(context)
        {
            db = context;
        }

        public async void Update(TeamModel model)
        {
            using (db)
            {
                db.Update(model);
                await db.SaveChangesAsync();
            }

        }
    }
}
