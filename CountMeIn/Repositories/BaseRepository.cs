﻿using AutoMapper;
using CountMeIn.Data;
using CountMeIn.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountMeIn.Repositories
{
    public class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        CountMeInDbContext db;

        public BaseRepository(CountMeInDbContext context)
        {
            db = context;
        }

        public virtual IEnumerable<T> GetAll()
        {
            using (db)
            {
                IEnumerable<T> models = db.Set<T>();
                return models;
            }
        }

        public virtual async Task<T> GetById(Guid Id)
        {
            using (db)
            {
                T model = await db.FindAsync<T>(Id);
                return model;
            }
        }

        public virtual async Task Add(T model)
        {
            using (db)
            {
                await db.AddAsync<T>(model);
                await db.SaveChangesAsync();
            }
        }

        public virtual async Task RemoveById(Guid Id)
        {
            using (db)
            {
                var model = await GetById(Id);
                
                db.Remove(model);
            }
        }

        
    }
}
