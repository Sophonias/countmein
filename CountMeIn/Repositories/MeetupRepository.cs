﻿using CountMeIn.Data;
using CountMeIn.Data.DataModels;
using CountMeIn.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountMeIn.Repositories
{
    public class MeetupRepository : BaseRepository<MeetupModel>, IMeetupRepository
    {
        CountMeInDbContext db;

        public MeetupRepository(CountMeInDbContext context) : base(context)
        {
            db = context;
        }

        public async void Update(MeetupModel model)
        {
            using (db)
            {
                db.Update(model);
                await db.SaveChangesAsync();
            }

        }

    }
}
