﻿using CountMeIn.Data.DataModels;

namespace CountMeIn.Repositories.Interfaces
{
    public interface ILeagueRepository
    {
        void Update(LeagueModel model);
    }
}