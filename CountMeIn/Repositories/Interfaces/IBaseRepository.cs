﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CountMeIn.Repositories.Interfaces
{
    public interface IBaseRepository<T> where T : class
    {
        Task Add(T model);
        IEnumerable<T> GetAll();
        Task<T> GetById(Guid Id);
        Task RemoveById(Guid Id);
    }
}