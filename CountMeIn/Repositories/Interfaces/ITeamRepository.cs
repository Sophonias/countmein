﻿using CountMeIn.Data.DataModels;

namespace CountMeIn.Repositories.Interfaces
{
    public interface ITeamRepository
    {
        void Update(TeamModel model);
    }
}