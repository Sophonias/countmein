﻿using CountMeIn.Data.DataModels;

namespace CountMeIn.Repositories.Interfaces
{
    public interface IMeetupRepository
    {
        void Update(MeetupModel model);
    }
}